#!/usr/bin/env bash

# Создаем симлинк на директорию проекта
echo "Symlink working directory to /var/www/ggt..."
ln -fs /vagrant /var/www/ggt

# Если нет виртуального окружения - создаем
if [ ! -d "/home/vagrant/.virtualenvs/ggt" ]; then
    echo "Create python virtual environment..."
    su vagrant -c "virtualenv --no-site-packages /home/vagrant/.virtualenvs/ggt/"
fi

# Устанавливаем необходимые пакеты через pip
echo "Install PIP packages..."
su vagrant -c "/home/vagrant/.virtualenvs/ggt/bin/pip install -r /var/www/ggt/requirements.txt"

# Создаем директорию для static-файлов
echo "Create static directory..."
su vagrant -c "mkdir -p /var/www/ggt/static"


echo "Bootstrap finished!"
