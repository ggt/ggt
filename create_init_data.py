# coding: utf-8
from __future__ import unicode_literals

from random import choice
from faker import Factory
from py2neo.neo4j import CypherQuery, GraphDatabaseService


def main():
    fake = Factory.create()
    graph_db = GraphDatabaseService()
    # add messages
    for i in xrange(10**4):
        if i % 1000 == 0:
            print i
        qs = "CREATE (m {text: {message}}) RETURN id(m)"
        result = CypherQuery(graph_db, qs).execute(message=fake.text())
        new_message_id = result.data[0].values[0]
        qs = "START n=node({id}) SET n.latest_comment={id}"
        CypherQuery(graph_db, qs).execute(id=new_message_id)
    # obtain messages id
    get_ids_qs = "START n=node(*) RETURN id(n)"
    result = CypherQuery(graph_db, get_ids_qs).execute()
    id_list = [i.values[0] for i in result.data]
    # add comments
    for i in xrange(10**6):
        if i % 10000 == 0:
            print i
        parent = choice(id_list)
        qs = "START n=node({id}) " \
             "CREATE (n)-[r:links_to]->(m {text: {message}}) " \
             "RETURN id(m)"
        kwargs = {
            'id': int(parent),
            'message': fake.text(),
        }
        result = CypherQuery(graph_db, qs).execute(**kwargs)
        new_message_id = result.data[0].values[0]
        qs = "START n=node({id}) MATCH m-[r:links_to*]->n  WHERE has(m.latest_comment) SET m.latest_comment={id};"
        CypherQuery(graph_db, qs).execute(id=new_message_id)
        id_list.append(new_message_id)
    print "Done"


if __name__ == "__main__":
    main()