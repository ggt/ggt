# coding: utf-8
from __future__ import unicode_literals

import os
from py2neo.neo4j import CypherQuery, GraphDatabaseService
from bottle import run, static_file, jinja2_view as view, route, request, redirect, auth_basic


GRAPH_DB = GraphDatabaseService()
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = os.path.join(BASE_DIR, 'static')


def add_path(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if isinstance(result, dict):
            result['request_path'] = request.path + '?' + request.query_string
        return result
    return wrapper


def check(user, passwd):
    if user == 'ggt' and passwd == 'fornex':
        return True
    return False


@route('/static/<filepath:path>')
def server_static(filepath):
    return static_file(filepath, root=STATIC_ROOT)


@route('/')
@auth_basic(check)
@view('home.html')
@add_path
def home():
    page = request.query.page
    if not page:
        page = 1
    page = int(page)
    skip = 30 * (page - 1)
    qs = "START n=node(*) WHERE has(n.latest_comment) RETURN count(n)"
    result = CypherQuery(GRAPH_DB, qs).execute()
    messages_count = result.data[0].values[0]
    has_next = (skip + 30) < messages_count
    qs = "START n=node(*) " \
         "WHERE has(n.latest_comment) " \
         "WITH id(n) as id, n.text as text, n.latest_comment as nlc " \
         "ORDER BY nlc DESC RETURN id, text SKIP {skip} LIMIT 30;"
    result = CypherQuery(GRAPH_DB, qs).stream(skip=skip)
    return {
        'page': page,
        'messages': result,
        'has_next': has_next,
        'messages_count': messages_count,
    }


@route('/message/<message_id:re:\d+>/')
@auth_basic(check)
@view('message.html')
@add_path
def message(message_id):
    message_id = int(message_id)
    page = request.query.page
    if not page:
        page = 1
    page = int(page)
    skip = 30 * (page - 1)
    qs = "START n=node({id}) MATCH (n)-[r:links_to]->x RETURN count(x)"
    result = CypherQuery(GRAPH_DB, qs).execute(id=message_id)
    comments_count = result.data[0].values[0]
    has_next = (skip + 30) < comments_count
    qs = "START n=node({id}) " \
         "RETURN {id} as id, n.text as text, 0 as parent_id, 0 as indent " \
         "UNION ALL " \
         "START n=node({id}) " \
         "MATCH (n)-[:links_to]->m WITH n, m SKIP {skip} LIMIT 30 "\
         "MATCH linksto=(n)-[r:links_to*]->x " \
         "WITH endNode(head(r)) as parent, m, x, linksto " \
         "WHERE parent=m " \
         "RETURN id(x) as id, x.text as text, id(parent) as parent_id, length(linksto) as indent "
    result = CypherQuery(GRAPH_DB, qs).stream(id=message_id, skip=skip)
    return {'messages': result, 'page': page, 'has_next': has_next, 'comments_count': comments_count}


@route('/message/add/')
@route('/message/<id:re:\d+>/add/')
@auth_basic(check)
@view('message_add.html')
@add_path
def message(id=None):
    message_text = request.query.message
    if message_text:
        if id:
            qs = "START n=node({id}) " \
                 "CREATE (n)-[r:links_to]->(m {text: {message}}) " \
                 "RETURN id(m)"
            kwargs = {
                'id': int(id),
                'message': message_text,
            }
            result = CypherQuery(GRAPH_DB, qs).execute(**kwargs)
            new_message_id = result.data[0].values[0]
            qs = "START n=node({id}) " \
                 "MATCH m-[r:links_to*]->n  " \
                 "WHERE has(m.latest_comment) " \
                 "SET m.latest_comment={id};"
            CypherQuery(GRAPH_DB, qs).execute(id=new_message_id)
        else:
            qs = "CREATE (m {text: {message}}) RETURN id(m)"
            result = CypherQuery(GRAPH_DB, qs).execute(message=message_text)
            new_message_id = result.data[0].values[0]
            qs = "START n=node({id}) SET n.latest_comment={id}"
            CypherQuery(GRAPH_DB, qs).execute(id=new_message_id)
        if request.query.is_ajax:
            return str(new_message_id)
        return redirect(request.query.next or '/message/{id}/'.format(id=new_message_id))
    return {'next': request.query.next}


run(host='0.0.0.0', port=8080, debug=True)